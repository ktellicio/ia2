#include <stdio.h>
#include "neural_network"

void Screen_Init(Screen * screen) {
	screen->matrix = malloc(DIM_X * (DIM_Y) * sizeof(State));
	screen->bias.state = active;
	screen->bias.weight = MAX_W;
}

void Screen_Set(uint8_t * value) {
	for(int i = 0, i < DYM_X, i++)
		for(int j = 0; j < DYM_Y; j++)
			screen->matrix[i][j] = value[i][j];
}

uint8_t update_Output(Neuron *n) {
	n->output = n->state * n->weight;
	return n->output;
}

uint8_t get_Biasing(Neuron *n) {
	return n->bias * MAX_W;
}

uint8_t is_Activated(Neuron *n) {
	if(n->output)
		return 1;
	else
		return 0; 
}

uint8_t is_Deactivated(Neuron *n) {
	if(n->output)
		return 0;
	else
		return 1; 		
}

uint32_t classification(Screen * screen) {
	uint32_t sum;
	for(int i = 0, i < DYM_X, i++)
		for(int j = 0; j < DYM_Y; j++) {
			if(mask[i][j])
				is_Activated(screen->matrix[i][j]);
			else
				is_Deactivated(screen->matrix[i][j]);

			sum = get_Biasing(screen->matrix[i][j]) + update_Output(screen->matrix[i][j]);
	}
	return sum;
}
