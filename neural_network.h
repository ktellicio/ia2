#define DIM_X 3
#define DIM_Y 4
#define WIDTH 1
#define MAX_W 100
uint8_t result;
// Letter l = classification();
// result << l;
enum {
	A,
	E,
	I,
	O,
	U,
	NUMBER_OF_LETTERS
} Leter;

enum {
	inactive,
	active
} State;

typedef struct {
	State state;
	State bias; 	// for the X0 = 1, the bias value
	uint8_t weight; // ranges from 0 to 100, which maps [0,100] -> [0,1]
	uint8_t output;
} Neuron;

typedef struct {
	State *matrix;
} Screen;

void Screen_Init(Screen * screen);

uint8_t update_Output(Neuron *n);
uint8_t get_Biasing(Neuron *n);

Letter classification(Screen * screen);

// TODO:
void training(NeuralNetwork * n);
